#!/bin/sh
export JAVA_HOME=/usr/lib/jvm/jre
cd `dirname $0`;

/usr/bin/systemctl stop `basename ./*.service`;

git pull;
mvn clean package;
chmod 755 ./target/*.jar
cp --force `find ./*.service -type f` /etc/systemd/system/
systemctl daemon-reload

/usr/bin/systemctl start `basename ./*.service`;