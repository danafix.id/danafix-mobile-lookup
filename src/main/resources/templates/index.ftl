<#import "/spring.ftl" as spring />

<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<@spring.url '/css/bootstrap.min.css'/>">
</head>
<body class="center">

<div class="container mt-5">
    <div class="row mt-20">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card mt-1">
                <div class="card-body">
                    <div class="row" id="container1">
                        <div class="col-12">
                            <h3>Mobile Lookup via Infobip</h3>
                        </div>

                        <div class="col-12">
                            <form id="form1" method="post" enctype='multipart/form-data' action="<@spring.url '/upload'/>">
                                <div class="form-group">
                                    <label for="file-data">Data file (.xlsx)</label>
                                    <input type="file" class="form-control-file" id="file-data" name="file-data">
                                </div>
                                <div class="form-group">
                                    <label for="file-email">Send result to email:</label>
                                    <input type="text" class="form-control" id="file-email" name="file-email" value="id.it@danafix.id">
                                </div>
                            </form>
                        </div>

                        <div class="col-12 text-center">
                            <button class="btn btn-primary" type="button" id="btn-start">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<script src="<@spring.url '/js/jquery-3.4.1.min.js'/>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<@spring.url '/js/bootstrap.min.js'/>"></script>

<script language="JavaScript" type="application/javascript">
$(document).ready(function () {
    $("#btn-start").click(function () {
        var data = new FormData($("#form1")[0]);

        $.ajax({
            url: "<@spring.url '/upload'/>",
            method: "POST",
            dataType: 'json',
            data: data,
            processData: false,
            contentType: false,
            success: function(result){
                if (!result.success) {
                    alert("Error "+result.msg);
                } else {
                    alert("Submitted successfully, please wait until finished!");
                    //window.location.href = "<@spring.url '/'/>";
                }
            },
            error: function(er){
            }
        });
    })
});
</script>

</body>
</html>