package danafix.mobile.lookup.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class DataUtil {
    @Autowired
    NamedParameterJdbcTemplate jdbc2;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final List<String> validStatus = Arrays.asList(new String[] {"DELIVERED_TO_HANDSET","NO_ERROR", "PENDING_ENROUTE", "REJECTED_NETWORK", "EC_ABSENT_SUBSCRIBER","EC_ABSENT_SUBSCRIBER_SM"});

    public void export(int logId) throws Exception {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("logId", logId);

        try {

            String sql = " SELECT id,msisdn,sent_time,sent_status_name,tag FROM mobile_lookup WHERE id=:logId  ";
            List<Map<String, Object>> rows = jdbc1.queryForList(sql, sqlParams);
            if (rows.size() <= 0) {
                log.error("Record #" + logId + " is not found!");
                return;
            }

            //jdbc1.update("UPDATE mobile_lookup SET exported")

            sqlParams.addValues(rows.get(0));
            String status = "" + sqlParams.getValue("sent_status_name");
            int isValid = (validStatus.contains(status)) ? 1 : 0;

            sqlParams.addValue("isValid", isValid);

            log.info("Exporting #" + logId);

            sql = " INSERT INTO [RiskReports].[dbo].[ID_Infobip_check_status] ([ID],[Check_dt],[Mobile_phone],[Status],[Is_valid],[Campaign_type]) " +
                    " VALUES (:id,:sent_time,:msisdn,:sent_status_name,:isValid,:tag) ";

            jdbc2.update(sql, sqlParams);
            log.info("ID #" + logId + " is exported!");
        } catch (org.springframework.dao.DuplicateKeyException e) {
            log.error("Duplicate data on server! #"+logId);
        } catch (Exception e) {
            log.error("Update error#"+logId+": "+e);
        }

    }
}
