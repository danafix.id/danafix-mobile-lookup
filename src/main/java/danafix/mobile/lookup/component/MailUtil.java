package danafix.mobile.lookup.component;

import danafix.mobile.lookup.service.MqMailer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class MailUtil {

    @Autowired
    MqUtil mqUtil;

    public void send(List<String> recipients, String subject, String body, List<String> attachments) throws Exception {
        JSONObject jsonMail = new JSONObject();

        jsonMail.put("recipients", recipients);
        jsonMail.put("attachments", attachments);
        jsonMail.put("subject", subject);
        jsonMail.put("body", body);

        mqUtil.sendText(MqMailer.QUEUE_MAIL_SMTP, jsonMail.toString());
    }

    public void send(String recipient, String subject, String body) throws Exception {
        List<String> recipients = new ArrayList<>();
        recipients.add(recipient);
        List<String> attachments = new ArrayList<>();
        send(recipients, subject, body, attachments);
    }

    public void send(String recipient, String subject, String body, String attachment) throws Exception {
        List<String> recipients = new ArrayList<>();
        recipients.add(recipient);
        List<String> attachments = new ArrayList<>();
        attachments.add(attachment);
        send(recipients, subject, body, attachments);
    }

    public void sendUsingTemplate(String templateId, JSONObject mailData, List<InputStream> attachments) throws Exception {

    }
}
