package danafix.mobile.lookup.service;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static danafix.mobile.lookup.config.ActiveMq.QUEUE_SEND_REQ;

public class LookupServiceSender {

    @Value("${infobip.url}")
    String infobipUrl;

    @Value("${infobip.user}")
    String infobipUser;

    @Value("${infobip.pass}")
    String infobipPass;

    @Value("${infobip.notifyUrl}")
    String notifyUrlTemplate;

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;

    String[] resendableStatus = {
    };

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_SEND_REQ)
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonReq = new JSONObject(jsonObjectString);
            log.info("Received req: "+jsonObjectString);

            int waitCount = 0;
            while (asyncExecutor.getActiveCount() > 35) {
                Thread.sleep(500);
                if (waitCount==0) log.info("Waiting for free thread ...");
                waitCount++;
            }

            asyncExecutor.execute(new LookupServiceSenderWorker(jsonReq));

        } catch (org.springframework.core.task.TaskRejectedException e ) {
            try {
                JSONObject jsonReq = new JSONObject(jsonObjectString);
                log.error("Queue is full, and task was rejected!");
                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("id", jsonReq.get("id"));
                String sql = " UPDATE mobile_lookup SET sent_time=NULL where id=:id ";
                jdt.update(sql, params);
            } catch (Exception e1) {
                log.error("Error rollback updating data! "+e,e);
            };
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    private class LookupServiceSenderWorker implements Runnable {
        private final JSONObject jsonReq;
        public LookupServiceSenderWorker(JSONObject jsonReq) {
            this.jsonReq = jsonReq;
        }

        @Override
        public void run() {
            sendRequest();
        }

        private void sendRequest() {
            Long id = jsonReq.getLong("id");
            String sentId = null;
            String sentStatus = null;
            String sentDescription = null;

            try {
                Map<String,String> textParams = new HashMap<>();
                textParams.put("id", ""+id);

                StringSubstitutor substitutor = new StringSubstitutor(textParams);
                String notifyUrl = substitutor.replace(notifyUrlTemplate);
                log.info("Notify URL: "+notifyUrl);

                JSONObject jsonHttpReq = new JSONObject();
                JSONArray mobileNumbers = new JSONArray();

                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                String rawMsisdn = jsonReq.getString("msisdn").replace(" ","");
                Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(rawMsisdn, "ID");

                String msisdn = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);

                mobileNumbers.put(msisdn);
                jsonHttpReq.put("to", mobileNumbers);
                jsonHttpReq.put("notifyUrl", notifyUrl);
                jsonHttpReq.put("notifyContentType", "application/json");

                log.info("JSON Request: "+jsonHttpReq);

                String authRaw = infobipUser + ":" + infobipPass;
                String authString = "Basic " + Base64.getEncoder().encodeToString(authRaw.getBytes());

                log.info("Infobip auth raw: "+authRaw+", encoded: "+authString);

                int timeout = 30;
                RequestConfig config = RequestConfig.custom()
                        .setConnectTimeout(timeout * 1000)
                        .setConnectionRequestTimeout(timeout * 1000)
                        .setSocketTimeout(timeout * 1000).build();
                CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

                HttpPost httpPost = new HttpPost(infobipUrl);
                StringEntity entity = new StringEntity(jsonHttpReq.toString());
                httpPost.setEntity(entity);
                httpPost.setHeader("Authorization", authString);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                CloseableHttpResponse response = client.execute(httpPost);
                String respText = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);

                client.close();

                JSONObject jsonResp = new JSONObject(respText).getJSONArray("results").getJSONObject(0);
                log.info(jsonResp.toString(4));

                JSONObject jsonStatus = jsonResp.getJSONObject("status");
                sentId = jsonResp.getString("messageId");
                sentStatus = jsonStatus.getString("name");
                sentDescription = jsonStatus.getString("description");

            } catch (Exception e) {
                sentStatus = "FAILED";
                sentDescription = ""+e;
            }

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id", id);
            params.addValue("sentStatus", sentStatus);
            params.addValue("sentId", sentId);
            params.addValue("sentDescription", sentDescription);

            String sql = " UPDATE mobile_lookup SET sent_time=NOW(), sent_id=:sentId, sent_status_name=:sentStatus, sent_description=:sentDescription WHERE id=:id ";
            jdt.update(sql, params);

            if (Arrays.asList(resendableStatus).contains(sentStatus)) {
                log.info("Set resend for ID: #"+id);
                sql = " UPDATE mobile_lookup SET sent_time=NULL, resend_on=ADDDATE(NOW(), INTERVAL 1 MINUTE) WHERE id=:id ";
                jdt.update(sql, params);
            }
        }
    }


}
