package danafix.mobile.lookup.service;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;

import static danafix.mobile.lookup.config.ActiveMq.QUEUE_SEND_UPDATE;

public class LookupServiceUpdater {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;

    @JmsListener(destination = QUEUE_SEND_UPDATE)
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonReq = new JSONObject(jsonObjectString);
            log.info("Received updater req: "+jsonObjectString);

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("resultRaw", jsonReq.getString("raw"));
            params.addValue("id", jsonReq.getLong("id"));

            //String sql = " UPDATE mobile_lookup_result_log SET req=NOW(), result_raw=:resultRaw WHERE id=:id ";
            String sql = " INSERT INTO mobile_lookup_result_log (req_id,result_raw) VALUES (:id, :resultRaw) ";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdt.update(sql, params, keyHolder);

            Long logId = keyHolder.getKey().longValue();

            JSONObject jsonResult = new JSONObject(jsonReq.getString("raw")).getJSONArray("results").getJSONObject(0);

            JSONObject jsonStatus = jsonResult.getJSONObject("status");
            JSONObject jsonError = jsonResult.getJSONObject("error");

            params.addValue("statusId", jsonStatus.get("id"));
            params.addValue("statusName", jsonStatus.get("name"));
            params.addValue("statusDescription", jsonStatus.get("description"));
            params.addValue("errorId", jsonError.get("id"));
            params.addValue("errorName", jsonError.get("name"));
            params.addValue("errorDescription", jsonError.get("description"));

            sql = " UPDATE mobile_lookup SET result_time=NOW(), result_status_id=:statusId, result_status_name=:statusName, result_status_description=:statusDescription, " +
                    " result_error_id=:errorId, result_error_name=:errorName, result_error_description=:errorDescription WHERE id=:id ";
            jdt.update(sql, params);

        } catch (Exception e) {
            log.error("Error update messsage.."+e,e);
        }
    }
}
