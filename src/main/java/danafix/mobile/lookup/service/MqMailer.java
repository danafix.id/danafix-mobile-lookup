package danafix.mobile.lookup.service;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.List;

@Component
public class MqMailer {
    @Value("${spring.mail.from}")
    private String from;

    @Autowired
    public JavaMailSender mailSender;

    @Autowired
    public static final String QUEUE_MAIL_SMTP = "infobip-lookup-mail-queue-smtp";

    private final static ThreadPoolTaskExecutor asyncExecutor = new ThreadPoolTaskExecutor();
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    static {
        log.info("Queue name: "+QUEUE_MAIL_SMTP);
        asyncExecutor.setMaxPoolSize(40);
        asyncExecutor.setQueueCapacity(5);
        asyncExecutor.setThreadNamePrefix("CSV-EXPORT-");
        asyncExecutor.initialize();
    }

    @JmsListener(destination = QUEUE_MAIL_SMTP)
    public void receiveMessage(@Payload String rawString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            log.info("Receiving message: "+rawString);
            final JSONObject jsonReq = new JSONObject(rawString);
            boolean retry = false;
            do {
                retry = false;
                try {
                    asyncExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                send(jsonReq);
                            } catch (Exception e) {
                                log.error("Error sending mail: "+e,e);
                            }
                        }
                    });
                    retry = false;
                } catch (Exception e) {
                    if (e instanceof org.springframework.core.task.TaskRejectedException) {
                        try {
                            //log.info("Maximum process limit ("+i+"/"+size+") (Queue: "+asyncExecutor.getThreadPoolExecutor().getQueue().size()+", active: "+asyncExecutor.getThreadPoolExecutor().getActiveCount()+")! retrying ...!");
                            Thread.sleep(300);
                        } catch (Exception e1) {};
                        retry = true;
                    }
                }
            } while (retry);

        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    public void send(JSONObject jsonObject) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

        List<Object> recipients = jsonObject.getJSONArray("recipients").toList();
        String[] to =  new String[recipients.size()];
        for (int i=0;i<recipients.size();i++) to[i] = ""+recipients.get(i);

        helper.setTo(to);
        helper.setFrom(from);
        helper.setSubject(jsonObject.getString("subject"));
        helper.setText(jsonObject.getString("body"), true);

        mailMessage.addHeader("Content-Transfer-Encoding", "quoted-printable");

        for (Object srcAttachment : jsonObject.getJSONArray("attachments").toList()) {
            File attachmentFile = new File(""+srcAttachment);
            helper.addAttachment(attachmentFile.getName(), attachmentFile);
        }

        boolean retry = false;
        int retryCount = 0;
        do {
            try {
                retry = false;
                mailSender.send(mailMessage);
                log.info("Email sent!");

                // remove attachement file ...
                for (Object srcAttachment : jsonObject.getJSONArray("attachments").toList()) {
                    File attachmentFile = new File(""+srcAttachment);
                    try {
                        FileUtils.forceDelete(attachmentFile);
                    } catch (Exception e) {};
                    //helper.addAttachment(attachmentFile.getName(), attachmentFile);
                }

            } catch (Exception e) {
                log.error("Error sending email: "+e,e);
                if (retryCount < 3) {
                    log.info("Retrying ...");
                    retry  = true;
                    retryCount++;
                }
            }
        } while(retry);
    }
}
