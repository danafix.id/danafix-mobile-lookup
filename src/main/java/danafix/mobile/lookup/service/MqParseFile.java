package danafix.mobile.lookup.service;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import danafix.mobile.lookup.component.MailUtil;
import danafix.mobile.lookup.component.MqUtil;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.crypto.KeyGenerator;
import javax.jms.Message;
import javax.jms.Session;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class MqParseFile {
    @Value("${infobip.url}")
    String infobipUrl;

    @Value("${infobip.user}")
    String infobipUser;

    @Value("${infobip.pass}")
    String infobipPass;

    @Value("${infobip.notifyUrl}")
    String notifyUrlTemplate;

    public final static String QUEUE_PARSE_FILE = "mobile-lookup-export-parse-file";

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;
    @Autowired
    MailUtil mailUtil;

    @Autowired
    NamedParameterJdbcTemplate jdt;

    @JmsListener(destination = QUEUE_PARSE_FILE)
    public void receiveMessage(@Payload String rawString, @Headers MessageHeaders headers, Message message, Session session) {
        String mailTo = "";
        File xlFile = null;
        String subject = "";
        String body = "";
        try {
            JSONObject jsonReq = new JSONObject(rawString);
            xlFile = new File(jsonReq.getString("file"));
            String originalName = jsonReq.getString("originalName");
            mailTo = jsonReq.getString("mailTo");
            subject = "Infobip Lookup Result - "+originalName;

            try {
                File outputFile = readAndSendFile(xlFile, originalName);
                body = "";
                mailUtil.send(mailTo, subject, body, outputFile.getAbsolutePath());
            } catch (Exception e) {
                log.error("Error parse file: "+e);
                body = ""+e;
                mailUtil.send(mailTo, subject, body, xlFile.getAbsolutePath());
            }
        } catch (Exception e) {
            log.error("Error: "+e,e);
        }
    }

    private File readAndSendFile(File xlFile, String originalName) throws Exception {
        log.info("Processing "+xlFile.getAbsolutePath());

        Workbook workbook = WorkbookFactory.create(xlFile);
        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        Map<String, Integer> headerMap = new HashMap<String,Integer>(); //Create map
        Row headerRow = sheet.getRow(0); //Get first row
//following is boilerplate from the java doc
        short minColIx = headerRow.getFirstCellNum(); //get the first column index for a row
        short maxColIx = headerRow.getLastCellNum(); //get the last column index for a row
        for(short colIx=minColIx; colIx<maxColIx; colIx++) { //loop from first to last index
            Cell headerCell = headerRow.getCell(colIx); //get the cell
            headerMap.put(headerCell.getStringCellValue().trim(),headerCell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
        }

        Integer mobileIdx = headerMap.get("MobilePhone");
        if (mobileIdx == null) mobileIdx = headerMap.get("Mobile");

        if (mobileIdx == null) throw new Exception("MobilePhone field not found!");

        int totalRows = sheet.getPhysicalNumberOfRows();
        for(int x = 1; x<=totalRows; x++){
            Row row = null;
            String mobile = "";
            try {
                row = sheet.getRow(x);
                Cell mobileCell = row.getCell(mobileIdx);
                if (mobileCell == null) {
                    log.warn("Empty cell, continue ...");
                    continue;
                }

                mobile = getStringCell(mobileCell);

                log.info(x + ". Mobile: " + mobile);
                processMobile(mobile, originalName, row);
                //break;
            } catch (org.springframework.dao.DuplicateKeyException e) {
                log.warn("Duplicate data!");
                try {
                    getStatusFromDb(mobile, row, originalName);
                } catch (Exception e2) {
                    log.error("Error get status from DB: "+e2,e2);
                }

                continue;
            } catch (Exception e) {
                log.error("Error: "+e);
            }
        }

        File xlResultFile = new File("/tmp/"+FilenameUtils.getBaseName(originalName)+" - result-"+DateTime.now().toString("yyMMdd_HHmmss")+".xlsx");

        FileOutputStream outputStream = new FileOutputStream(xlResultFile);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        // send the file!

        return xlResultFile;

    }

    private void getStatusFromDb(String mobile, Row row, String originalName) throws Exception {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("mobile", mobile);
        sqlParams.addValue("tag", originalName);

        String sql = " SELECT result_status_name,result_status_description FROM mobile_lookup WHERE msisdn=:mobile AND tag=:tag ";
        List<Map<String,Object>> rows = jdt.queryForList(sql, sqlParams);
        if (rows.size()<=0) return;

        String status = ""+rows.get(0).get("result_status_name");
        String description = ""+rows.get(0).get("result_status_description");

        int lastCellIdx = row.getLastCellNum();
        Cell resultCell = row.createCell(lastCellIdx);
        resultCell.setCellValue(status);
    }

    private void processMobile(String mobile, String originalName, Row row) throws Exception {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(mobile, "ID");

        mobile = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164).replaceAll("[^0-9]","");

        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("mobile", mobile);
        sqlParams.addValue("tag", originalName);

        String sql = " INSERT INTO mobile_lookup (msisdn,tag) VALUES (:mobile,:tag) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdt.update(sql, sqlParams, keyHolder);

        long logId = keyHolder.getKey().longValue();

        String status = send(logId, mobile);

        int lastCellIdx = row.getLastCellNum();
        Cell resultCell = row.createCell(lastCellIdx);
        resultCell.setCellValue(status);
    }

    private String getStringCell(Cell cell) {
        switch (cell.getCellType()) {
            case BOOLEAN: return ""+cell.getBooleanCellValue();
            case STRING: return cell.getRichStringCellValue().getString();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return (new DateTime(cell.getDateCellValue())).toString("yyyy-MM-dd HH:mm:ss");
                } else {
                    return ""+(new Double(cell.getNumericCellValue())).longValue();
                }
            case FORMULA: return ""+cell.getCellFormula();
            default:
                return "";
        }
    }

    private String send(long logId, String mobile) throws Exception {
        JSONObject jsonReq = new JSONObject();
        JSONArray mobileNumbers = new JSONArray();

        String sentStatus;
        String sentDescription;
        String sentId = null;

        try {
            mobileNumbers.put(mobile);
            jsonReq.put("to", mobileNumbers);

            String authRaw = infobipUser + ":" + infobipPass;
            String authString = "Basic " + Base64.getEncoder().encodeToString(authRaw.getBytes());

            int timeout = 30;
            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(timeout * 1000)
                    .setConnectionRequestTimeout(timeout * 1000)
                    .setSocketTimeout(timeout * 1000).build();
            CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

            HttpPost httpPost = new HttpPost(infobipUrl);
            StringEntity entity = new StringEntity(jsonReq.toString());
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", authString);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);
            String respText = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);

            client.close();

            JSONObject jsonResp = new JSONObject(respText).getJSONArray("results").getJSONObject(0);
            log.info(jsonResp.toString());

            JSONObject jsonStatus = jsonResp.getJSONObject("status");
            sentStatus = jsonStatus.getString("name");
            sentDescription = jsonStatus.getString("description");
        } catch (Exception e) {
            sentStatus = "FAILED";
            sentDescription = ""+e;
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", logId);
        params.addValue("sentStatus", sentStatus);
        params.addValue("sentId", sentId);
        params.addValue("sentDescription", sentDescription);

        String sql = " UPDATE mobile_lookup SET sent_time=NOW(), sent_id=:sentId, sent_status_name=:sentStatus, sent_description=:sentDescription,result_status_name=:sentStatus,result_status_description=:sentDescription " +
                " WHERE id=:id ";
        jdt.update(sql, params);

        return sentStatus;
    }
}
