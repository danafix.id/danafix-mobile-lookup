package danafix.mobile.lookup.service;

import danafix.mobile.lookup.component.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;

@Component
public class ServiceUpdater implements ApplicationRunner  {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    DataUtil dataUtil;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                getAndSend();
            }
        }, 1800000);
    }

    private void getAndSend() {
        try {
            boolean retry = false;
            do {
                retry = false;
                MapSqlParameterSource sqlParams = new MapSqlParameterSource();
                String sql = " SELECT id FROM mobile_lookup WHERE exported IS NULL AND sent_time IS NOT NULL AND sent_status_name IS NOT NULL order by id LIMIT 0,20 ";

                List<Long> ids = jdt.queryForList(sql, sqlParams, Long.class);

                for (Long id : ids) {
                    if (!retry) retry = true;
                    send(id.intValue());
                }
            } while (retry);
        } catch (Exception e) {
            log.error("Error retrieve log: "+e);
        }
    }

    private void send(int logId) {
        boolean retry = false;
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("logId", logId);
        jdt.update(" UPDATE mobile_lookup SET exported=NOW() WHERE id=:logId ", sqlParams);
        do {
            retry = false;
            try {
                log.info("Processing #"+logId);
                asyncExecutor.execute(new ServiceUpdaterWorker(logId));
            } catch (org.springframework.core.task.TaskRejectedException e ) {
                retry = true;
                try {
                    log.warn("Retrying .... ");
                     Thread.sleep(3000);
                } catch (Exception e2) {};
            } catch (Exception e) {
                retry = false;
            }
        } while (retry);

    }

    private class ServiceUpdaterWorker implements Runnable {
        private final int logId;

        public ServiceUpdaterWorker(int logId) {
            this.logId = logId;
        }

        @Override
        public void run() {
            try {
                dataUtil.export(logId);
            } catch (Exception e) {
                log.error("Error update: "+e);
            }
        }
    }
}
