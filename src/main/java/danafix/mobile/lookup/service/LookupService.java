package danafix.mobile.lookup.service;

import danafix.mobile.lookup.component.MqUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static danafix.mobile.lookup.config.ActiveMq.QUEUE_SEND_REQ;


public class LookupService implements ApplicationRunner {
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MqUtil mqUtil;


    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new LookupServiceWorker(), 3000);
    }


    private class LookupServiceWorker implements Runnable {
        @Override
        public void run() {
            try {
                MapSqlParameterSource params = new MapSqlParameterSource();
                String sql = "";

                int ct = 0;
                sql = " SELECT count(*) FROM `mobile_lookup` WHERE 1 AND sent_time IS NOT NULL AND sent_status_name IS NULL ";
                ct = jdt.queryForObject(sql, params, Integer.class);
                while (ct > 1000) {
                    Thread.sleep(10000);
                    ct = jdt.queryForObject(sql, params, Integer.class);
                    log.warn("Waiting for updating database ....");
                 }

                sql = " SELECT * FROM mobile_lookup WHERE sent_time IS NULL AND (sch_on IS NULL OR sch_on <=NOW())" +
                        " AND (resend_on <=NOW() OR resend_on IS NULL) AND resend_count <4 " +
                        " ORDER BY resend_count, id LIMIT 0,300 ";
                List<Map<String,Object>> rows = jdt.queryForList(sql, params);

                for (Map<String,Object> row : rows) {
                    log.info("Processing id: "+row.get("id")+", tag: "+row.get("tag")+", to: "+row.get("msisdn")+", resend: "+row.get("resend_count"));
                    final JSONObject jsonReq = new JSONObject(row);

                    params.addValue("id", row.get("id"));
                    sql = " UPDATE mobile_lookup SET sent_time=NOW(), resend_count=resend_count+1 WHERE id=:id ";
                    jdt.update(sql, params);

                    //mqUtil.send(QUEUE_SEND_REQ, jsonReq);
                }
            } catch (Exception e) {
                log.error("Error while getting queue: "+e,e);
            }
        }
    }
}
