package danafix.mobile.lookup.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class ActiveMq {
    public final static String QUEUE_SEND_REQ = "queue-send-request";
    public final static String QUEUE_SEND_UPDATE = "queue-send-updater";
}
