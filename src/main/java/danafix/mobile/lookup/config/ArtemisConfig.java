package danafix.mobile.lookup.config;

import org.apache.activemq.artemis.jms.client.ActiveMQJMSConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.jms.Connection;
import javax.jms.Session;
import java.util.Properties;

@Configuration
@Import(ApplicationProperties.class)
public class ArtemisConfig {

    public static final String QUEUE_TEST = "queue-test-only";

    @Autowired
    Properties appProperties;

    @Bean(name = "mqFactory")
    public ActiveMQJMSConnectionFactory getMqFactory() {
        String host = appProperties.getProperty("artemis.host");
        int port = Integer.parseInt(appProperties.getProperty("artemis.port"));
        String user = appProperties.getProperty("artemis.user");
        String password = appProperties.getProperty("artemis.password");

        ActiveMQJMSConnectionFactory factory = new ActiveMQJMSConnectionFactory("tcp://"+host+":"+port);
        factory.setUser(user).setPassword(password);
        factory.setReconnectAttempts(-1);
        factory.setThreadPoolMaxSize(100);

        return factory;
    }

    @Bean(name="mqConn")
    public Connection getMqConnection() {
        try {
            Connection conn = getMqFactory().createConnection();
            conn.start();
            return conn;
        } catch (Exception e) {
            return null;
        }
    }

    @Bean(name="mqSession")
    public Session getMqSession() {
        try {
            Session session = getMqConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);

            return session;
        } catch (Exception e) {
            return null;
        }
    }
}
