package danafix.mobile.lookup.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Properties;

public class ApplicationProperties {
    @Bean(name="appProperties")
    public Properties getLocalProperties() {
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        Resource yamlResource = new ClassPathResource("application.yml");
        yaml.setResources(yamlResource);
        Properties appProperties = yaml.getObject();
        return appProperties;
    }
}
