package danafix.mobile.lookup.controller;

import danafix.mobile.lookup.component.MqUtil;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;

import static danafix.mobile.lookup.config.ActiveMq.QUEUE_SEND_UPDATE;

@RestController
public class RestApi {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;

    @RequestMapping(value = "/result/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String getRequest(HttpServletRequest request, @PathVariable("id") Long id) {

        try {
            String reqBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
            log.info("Request id: "+id+", body: "+reqBody);

            JSONObject jsonReq = new JSONObject();
            jsonReq.put("id", id);
            jsonReq.put("raw", reqBody);

            //mqUtil.send(QUEUE_SEND_UPDATE, jsonReq);

        } catch (Exception e) {
            log.error("Error retrieving request body!");
        }

        return "{}";
    }
}
