package danafix.mobile.lookup.controller;

import danafix.mobile.lookup.component.MqUtil;
import danafix.mobile.lookup.service.MqParseFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.Iterator;

@Controller
public class Default {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;

    @GetMapping(value = {"/"})
    public String index(ModelMap model, HttpServletRequest request) {

        return "index";
    }

    @ResponseBody
    @PostMapping(value = {"/upload"})
    public String upload(ModelMap model, MultipartHttpServletRequest request, HttpServletResponse response) {

        boolean success = false;
        String msg = "";
        try {

            Iterator<String> fileNames = request.getFileNames();
            while (fileNames.hasNext()) {
                log.info("File name: "+fileNames.next());
            }

            MultipartFile fileData = request.getFile("file-data");

            log.info("Data file: "+fileData.getOriginalFilename());

            if (!FilenameUtils.getExtension(fileData.getOriginalFilename()).toLowerCase().equals("xlsx")) throw new Exception("Wrong data file!");

            File csvFile = File.createTempFile(fileData.getOriginalFilename()+" - ",".xlsx");
            FileUtils.writeByteArrayToFile(csvFile, fileData.getBytes(), false);

            mqUtil.sendJson(
                    MqParseFile.QUEUE_PARSE_FILE,
                    new JSONObject()
                            .put("mailTo", request.getParameter("file-email"))
                            .put("originalName", fileData.getOriginalFilename())
                            .put("file", csvFile.getAbsolutePath())

            );

            success = true;
        } catch (Exception e) {
            success = false;
            msg = e.getMessage();
            log.error("Error: "+e);
        }

        response.setHeader("Content-type", "application/json");
        return new JSONObject()
                .put("success", success)
                .put("msg", msg)
                .toString();
    }
}
